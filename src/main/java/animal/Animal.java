package animal;

public class Animal {
    private String name;
    private String speecies="";
    private int age;

     public Animal(String nameParameter, String species, int age){
        name =nameParameter;
        this.speecies=species;
        this.age = age;
    }
    public String getName(){
         return name;
    }
    public String getSpeecies(){
         return speecies;
    }
    public int getAge(){
         return age;
    }
    public void setName(String newName){
         this.name= newName;
    }

    public void setSpeecies(String speecies) {
        this.speecies = speecies;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
